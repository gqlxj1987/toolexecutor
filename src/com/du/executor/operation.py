#encoding:utf-8
'''
Created on 2013-8-3
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''
import logging
import os
import subprocess
import sys
use_shell = sys.platform.startswith( "win" )
class Operation(object):
    def __init__(self,name,
                 base_path=None,
                 logger=None,
                 next_operation=None):
        logger_name =__name__+".Operation"
        self.logger = logger or logging.getLogger(logger_name)
        self.base_path = base_path
        self.command = []
        self.name = name
        self.next_operation = next_operation
    def set_base_path(self,base_path):
        self.base_path = base_path
    def add_command(self,command):
        self.command.append(command)
        return self
    def add_option(self,option):
        self.command.append(option)
        return self
    def get_name(self):
        return self.name
    def set_name(self,name):
        self.name = name
    def do_execute(self):
        os.chdir(self.base_path)
        return self.__run_cmd(self.command)
    def get_next_operation(self):
        return self.next_operation
    def set_next_operation(self,operation):
        self.next_operation = operation
    def clean_command(self):
        self.command = []
        
    def __run_cmd(self,command,
                  universal_newlines = True,
                  env = os.environ ):
        self.logger.info("run cmd with[%s]"%(' '.join(command)))
        result = subprocess.Popen( command, stdout = subprocess.PIPE, stderr = subprocess.PIPE,
                         shell = use_shell, universal_newlines = universal_newlines,
                         env = env )
        output = result.stdout.read()
        result.wait()
        errout = result.stderr.read()
        if errout:
            self.logger.error('fail to run cmd with error [%s]'%errout)
            return errout,False
        return output,True
