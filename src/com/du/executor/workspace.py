#encoding:utf-8
'''
Created on 2013-8-3
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''
from com.du.common.httputils import HttpService
import logging
import os
import shutil
import uuid
class IWorkspace(object):
    
    def init_env(self,config=None):
        raise NotImplementedError( "abstract method -- subclass %s must override" % self.__class__ )
    def init_files(self,files_dict):
        raise NotImplementedError( "abstract method -- subclass %s must override" % self.__class__ )
    def clean(self):
        raise NotImplementedError( "abstract method -- subclass %s must override" % self.__class__ )
    def execute(self,operation):
        raise NotImplementedError( "abstract method -- subclass %s must override" % self.__class__ )


class HttpWorkspace(IWorkspace):
    SELECT_KEY = "http"
    def __init__(self,root_path,name,logger=None,unique_key=None):
        self.logger = logger or logging.getLogger(__name__)
        self.init_status = False
        if not os.path.exists(root_path):
            self.logger.error('home path(%s) is not existing'%root_path)
            self.init_status = False
        if unique_key:
            real_path = unique_key+name
        else:
            real_path = str(uuid.uuid1())+name
        if not name:
            self.name = real_path
        self.name = name
        self.real_path = root_path+'/'+real_path
        if not os.path.exists(self.real_path):
            os.makedirs(self.real_path)
        self.root_path = root_path
        self.http_service = HttpService()
        self.should_cleaned = []
    def init_env(self):
        print ''
    def init_files(self,files_dict):
        for file_name in files_dict.keys():
            head,tail = os.path.split(file_name)
            if not os.path.exists(self.real_path+'/'+head):
                os.makedirs(self.real_path+'/'+head)
            content,status = self.http_service.get(files_dict[file_name])
            if not status:
                self.logger.error('fail to download file(%s) with exception(%s)'%(files_dict[file_name],content))
                return False
            try:
                full_file_path = self.real_path+'/'+file_name
                temp_file = open(full_file_path,'w')
                temp_file.write(content)
                temp_file.close()
                self.should_cleaned.append(full_file_path)
            except Exception,ex:
                self.logger.error('fail to write file(%s) with %s'%(file_name,str(ex)))
                return False
        return True
    def clean(self):
        try:
            os.chdir(self.root_path)
            shutil.rmtree(self.real_path)
        except Exception,ex:
            self.logger.error('fail to clean workspace (%s) with %s '%(self.real_path,str(ex)))
    def execute(self,operation):
        result = {}
        if not operation:
            return
        current_operation = operation
        while current_operation:
            current_operation.set_base_path(self.real_path)
            result[current_operation]=current_operation.do_execute()
            current_operation = current_operation.get_next_operation()
        return result

class Selector():
    @classmethod
    def select(cls,protocol):
        workspace_dict = {HttpWorkspace.SELECT_KEY:HttpWorkspace}
        if workspace_dict.has_key(protocol):
            return workspace_dict[protocol]
        return None