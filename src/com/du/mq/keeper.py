#encoding:utf-8
'''
Created on 2013-7-30
@author: innerp
'''
import logging
import socket
import threading
import time
class MQKeeper(threading.Thread):
    def __init__(self,target,host_list,heat_beat_api=None,logger=None,retry_times=100):
        threading.Thread.__init__(self)
        self.logger = logger or logging.getLogger(__name__)
        self.target = target
        self.host_list = host_list
        self.running = True
        self.setName("mqkeeper")
        self.retry_times=retry_times
        self.setDaemon(False)
    def monitoring(self):
        count=0
        while self.running:
            time.sleep(5)
            if self.target.check_status():
                continue
            self.logger.warn('host(%s) is down '%self.target.get_host())
            host,port = self.get_avilable_host()
            if not host:
                if self.retry_times==-1:
                    continue
                if count<self.retry_times:
                    count+=1
                    continue
                self.running = False
                self.logger.error('sorry ,no host is avilable,we will exit the process')
                return ;
            self.target.change_host(host,port)
            self.target.retart()
            
            
    def get_avilable_host(self):
        test_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        for host,port in self.host_list:
            try:
                test_socket.connect((host,port))
                test_socket.close()
                return host,port
            except Exception,ex:
                self.logger.error('host(%s) is lost'%host)
                pass
        return None,None
    def close(self):
        self.running = False
    def run(self):
        self.logger.info('start monitor');
        self.monitoring()
