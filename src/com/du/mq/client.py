#coding:utf-8
'''
Created on 2013-4-11

@author: Wong
@copyright: imotai.duapp.com
'''

from com.du.jobs.worker import Job
from com.du.mq.frame import ConnectFrame, MessageFrame, DisconnectFrame, \
    SubscribeFrame, SendFrame
from com.du.mq.mqclientscoket import MQSocket
import logging
import threading
import time
class MQClient(threading.Thread):
    def __init__(self,host,port,user,password,client_id=None,logger=None):
        threading.Thread.__init__(self)
        self.logger = logger or logging.getLogger(__name__)
        self.host = host 
        self.user = user
        self.password = password
        self.port =port
        self.mqscoket = MQSocket()
        self.connected=False
        self.running = False
        if client_id:
            self.client_id =client_id
        self.setName("mqclient")
    def __init_connect(self):
        self.logger.info('INIT SOCKET CONNECT ,host->%s,port->%s'%(self.host,str(self.port)))
        self.mqscoket.connect(self.host, self.port)
        if self.client_id:
            connect_frame = ConnectFrame(self.host,self.user,self.password,self.client_id)
        else:
            connect_frame = ConnectFrame(self.host,self.user,self.password)
        self.mqscoket.send_message(connect_frame)
        data=self.mqscoket.receive_message(MQSocket.MESSAGE_MAX_LENGTH)
        message_frame = MessageFrame(data)
        if message_frame.get_command()=='CONNECTED':
            self.logger.info('CONNECT TO ACTIVEMQ SUCCESSFULLY ')
            self.connected = True
            self.running = True
        else:
            self.logger.error(data)
    def __init__disconnect(self):
        self.logger.info('START TO DISCONNECT')
        disconnect_frame = DisconnectFrame(9527)
        self.mqscoket.send_message(disconnect_frame)
        #等待server回复已处理完发送的消息
        data = self.mqscoket.receive_message(MQSocket.MESSAGE_MAX_LENGTH)
        message_frame = MessageFrame(data)
        if (message_frame.get_header().has_key("receipt-id") 
            and message_frame.get_header()['receipt-id']=='9527'):
            self.logger.info('DISCONNECT SUCCESSFULLY')
        else:
            self.logger.error('FAIL TO DISCONNECT CONNECTIONG')
    def subscribe(self,destination,subscribe_id,pool,call_back,subscribe_name=None):
        self.destination = destination
        self.pool = pool
        self.call_back = call_back
        self.subscribe_name =subscribe_name
        self.subscribe_id = subscribe_id
        def should_run_func():
            subscribe_frame = SubscribeFrame(self.destination,self.subscribe_id,subscribe_name=self.subscribe_name)
            self.mqscoket.send_message(subscribe_frame)
            data_buffer = []
            while self.running:
                try:
                    data = self.mqscoket.receive_message(1)
                    data_buffer.append(data)
                    if not data.endswith('\x00'):
                        continue
                    message_frame = MessageFrame(''.join(data_buffer))
                    data_buffer = []
                    self.logger.info(message_frame.get_command())
                    if message_frame.get_command()!='ERROR':
                        job = Job(self.call_back,message_frame.get_body())
                        self.pool.add_task(job)
                    else:
                        self.logger.error(data)
                        self.connected = False
                        self.running = False
                except Exception,ex:
                    self.logger.error('runtime error %s'%str(ex))
                    self.connected = False
                    self.running = False
        self.should_run_func = should_run_func
    def send(self,body,destination):
        try:
            send_frame = SendFrame(destination,body)
            self.mqscoket.send_message(send_frame)
        except RuntimeError:
            self.connected = False
            self.running = False
    def close_connection(self):
        self.running = False
        time.sleep(2)
        self.__init__disconnect()
        self.mqscoket.close()
        self.connected = False
    def run(self):
        try:
            if self.mqscoket:
                self.mqscoket.close()
            self.mqscoket = MQSocket()
            self.__init_connect()
            self.should_run_func()
        except Exception,ex:
            self.logger.error('error happens %s'%str(ex))
            pass
    def check_status(self):
        return self.connected
    def change_host(self,host,port):
        self.host = host
        self.port = port
    def retart(self):
        self.logger.info('restarting mqclient')
        self.run()
    def get_host(self):
        return self.host
