#encoding:utf-8
'''
Created on 2013-8-3
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''
from com.du import settings
from com.du.comment.uploader import CommentUploader
from com.du.common.timeutil import Stopwatch
from com.du.executor.operation import Operation
from com.du.executor.workspace import Selector
from com.du.jobs.workerpool import WorkerPool
from com.du.mq.client import MQClient
from com.du.mq.keeper import MQKeeper
import json
import logging
import time
def process_msg(msg):
    msg_object = json.loads(str(msg))
    home_path=settings.HOME_PATH
    workspace_class = Selector.select(msg_object['protocol'])
    workspace = workspace_class(home_path,msg_object['name'])
    init_statue = workspace.init_files(msg_object['files'])
    if not init_statue:
        workspace.clean()
        return
    operation = Operation(name="svncommand")
    operation.add_command(msg_object['command'])
    for option in msg_object['options']:
        operation.add_option(option)
    results = workspace.execute(operation)
    uploader =CommentUploader(msg_object['call_back'],msg_object['tool_id'],\
                        msg_object['issue'],results)
    uploader.upload()
    workspace.clean()
def real_main():
    logger = logging.getLogger(__name__)
    stopwatch = Stopwatch()
    stopwatch.start()
    pool = WorkerPool(10)
    host_list = []
    for host in settings.ACTIVEMQ_HOST.split(","):
        host_list.append((host.strip(),settings.ACTIVEMQ_STOMP_PORT))
    mqclient = MQClient(host_list[0][0],host_list[0][1],settings.USER_NAME,settings.PASSWORD,settings.CLIENT_ID)
    mqclient.subscribe('/queue/coodertool',9875,pool,process_msg)
    mqclient.start()
    #give sometime to mqclient to init connection
    time.sleep(10)
    keeper = MQKeeper(mqclient,host_list,retry_times=10)
    keeper.start()
    stopwatch.stop()
    logger.info('executor is started with %s consumed'%stopwatch.pretty_formate())
