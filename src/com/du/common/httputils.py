#encoding:utf-8
'''
Created on 2013-8-3
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''
import logging
import socket
import urllib
import urllib2
class HttpService():
    def __init__(self,logger=None):
        self.logger = logger or logging.getLogger(__name__)
    def post(self,url,params):
        return self.__service(url, params)
    def get(self,url):
        return self.__service(url)
    def __service(self,url,params=None,timeout=50):
        old_timeout = socket.getdefaulttimeout()
        socket.setdefaulttimeout( timeout )
        try:
            #POST
            if params:
                self.logger.debug('post %s params[%s]'%(url,params))
                request = urllib2.Request( url, urllib.urlencode(params) )
            #GET
            else:
                self.logger.debug('get %s params[%s]'%(url,params))
                request = urllib2.Request( url )
            request.add_header( 'Accept-Language', 'zh-cn' )
            response = urllib2.urlopen( request )
            content = response.read()
            response.close()
            self.logger.debug('content->%s,code->%d'%(content,response.code))
            if response.code==200:
                return content,True
            return content,False
        except Exception,ex:
            return str(ex),False
        finally:
            socket.setdefaulttimeout( old_timeout )