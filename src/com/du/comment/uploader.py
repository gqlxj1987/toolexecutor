#encoding:utf-8
'''
Created on 2013-8-4
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''
from com.du.common.httputils import HttpService
import logging
import json
class CommentUploader(object):
    def __init__(self,call_back_api,tool_id,issue_id,results,logger=None):
        self.call_back_api = call_back_api
        self.tool_id = tool_id
        self.results = results
        self.issue_id = issue_id
        self.logger = logger or logging.getLogger(__name__)
    def formate_msg(self,results,tool_id):
        json_datas= {'tool_id':tool_id}
        json_datas['issue'] = self.issue_id
        for key in results.keys():
            output,status = results[key]
            if not status:
                continue
            lines = output.splitlines()
            for line in lines:
                line_parts = line.split(':')
                if  len(line_parts)<5:
                    continue
                type = line_parts[0]
                file_name = line_parts[1]
                line_no = line_parts[2]
                if json_datas.has_key(file_name):
                    json_datas[file_name]['newcode'].append({str(line_no):':'.join(line_parts[3:]),'type':type})
                else:
                    new_item = {}
                    new_code = []
                    new_code.append({str(line_no):':'.join(line_parts[3:]),'type':type})
                    new_item['newcode'] = new_code
                    json_datas[file_name]= new_item
        return json_datas
    def upload(self):
        params = {}
        params['json_data'] = json.dumps(self.formate_msg(self.results,self.tool_id))
        params['token'] = 'd2c1a71c-5af0-11e2-a48c-3440b5aaa45e'
        http_service = HttpService()
        result_str,success = http_service.post(self.call_back_api, params)
        if not success:
            self.logger.error('fail to upload comment with exception %s'%str(result_str))
        self.logger.info(result_str)