#encoding:utf-8
'''
Created on 2013-8-3
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''
from com.du.jobs.worker import Worker
from Queue import Queue
class WorkerPool(object):
    def __init__(self,pool_size):
        self.tasks = Queue(pool_size)
        for index in range(pool_size):
            Worker(self.tasks,'worker'+str(index))
    def add_task(self,job):
        self.tasks.put(job)
