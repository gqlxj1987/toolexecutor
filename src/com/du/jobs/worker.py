#encoding:utf-8
'''
Created on 2013-8-3
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''
from com.du.common.timeutil import Stopwatch
from threading import Thread
import logging
class Worker(Thread):
    def __init__(self,tasks,name,logger=None):
        Thread.__init__(self)
        self.tasks = tasks
        self.name = name
        self.setName(name)
        #when main thread exits,the thread will auto-exit
        self.setDaemon(True)
        self.logger = logger or logging.getLogger(__name__)
        self.start()
    def run(self):
        while True:
            job= self.tasks.get()
            try:
                stopwatch = Stopwatch()
                stopwatch.start()
                job.execute()
                stopwatch.stop()
                self.logger.info('the time which job consumed is %s'%stopwatch.pretty_formate())
            except Exception,ex:
                self.logger.error('worker[%s],error[%s]'%(self.name,str(ex)))
class Job(object):
    def __init__(self,func,*args,**kargs):
        self.func = func
        self.args = args
        self.kargs = kargs
    def execute(self):
        self.func(*self.args,**self.kargs)