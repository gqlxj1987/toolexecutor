#encoding:utf-8
'''
Created on 2013-8-4
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''


import logging.config
import os
import sys
if __name__=="__main__":
    sys.path.append(os.sep.join([os.getcwd(),'src']))
    from com.du.facade import real_main
    from com.du.settings import LOGGER
    logging.config.dictConfig(LOGGER)
    try:
        real_main()
    except KeyboardInterrupt:
        sys.exit( 1 )
    