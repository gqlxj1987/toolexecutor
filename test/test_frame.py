'''
Created on 2013-8-9

@author: Wong
'''
from com.du.mq.frame import ConnectFrame
import unittest


class Test(unittest.TestCase):


    def test_ConnectFrame_only_host(self):
        connect_frame = ConnectFrame('127.0.0.1')
        lines = connect_frame.frame_to_str().splitlines()
        self.assertEqual(5, len(lines))
        self.assertEqual("CONNECT",lines[0])
        self.assertEqual("host:127.0.0.1",lines[1])
    def test_ConnectFrame_with_user(self):
        connect_frame = ConnectFrame(host='127.0.0.1',login="system",password="manager")
        lines = connect_frame.frame_to_str().splitlines()
        self.assertEqual(7, len(lines))
        self.assertEqual("CONNECT",lines[0])
        self.assertEqual("passcode:manager",lines[1])
        self.assertEqual("login:system",lines[2])
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_ConnectFrame']
    unittest.main()