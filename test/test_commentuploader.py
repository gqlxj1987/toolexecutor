'''
Created on 2013-8-5

@author: Wong
'''
from com.du.comment.uploader import CommentUploader
import json
import unittest


class Test(unittest.TestCase):


    def testName(self):
        result ={"key":("""FATAL:A179.cpp:-1:B1002:file name should be lowercase, with underscores between words
FATAL:A179.cpp:2:B1007:At least two levels of namespace are needed, and top level should be namespace 'baidu'.
FATAL:A179.cpp:7:A179:the name of function should be lowercase, with underscores between words current: My_function2
FATAL:A179.cpp:8:A179:the name of function should be lowercase, with underscores between words current: MyFunction3
FATAL:A179.cpp:9:A179:the name of function should be lowercase, with underscores between words current: MyFunction4
FATAL:A179.cpp:14:A179:the name of function should be lowercase, with underscores between words current: MyFunction3
FATAL:A179.cpp:14:B1007:At least two levels of namespace are needed, and top level should be namespace 'baidu'.
FATAL:A179.cpp:17:B1007:At least two levels of namespace are needed, and top level should be namespace 'baidu'.
FATAL:folder/A1791.cpp:-1:B1002:file name should be lowercase, with underscores between words
FATAL:folder/A1791.cpp:2:B1007:At least two levels of namespace are needed, and top level should be namespace 'baidu'.
FATAL:folder/A1791.cpp:7:A179:the name of function should be lowercase, with underscores between words current: My_function2
FATAL:folder/A1791.cpp:8:A179:the name of function should be lowercase, with underscores between words current: MyFunction3
FATAL:folder/A1791.cpp:9:A179:the name of function should be lowercase, with underscores between words current: MyFunction4
FATAL:folder/A1791.cpp:14:A179:the name of function should be lowercase, with underscores between words current: MyFunction3
FATAL:folder/A1791.cpp:14:B1007:At least two levels of namespace are needed, and top level should be namespace 'baidu'.
FATAL:folder/A1791.cpp:17:B1007:At least two levels of namespace are needed, and top level should be namespace 'baidu'.""",True)}
        uploader = CommentUploader('api','tool_id','issue_id','result')
        print json.dumps(uploader.formate_msg(result, 'tool_id'))
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()