'''
Created on 2013-8-3

@author: Wong
'''
from com.du.executor.operation import Operation
from com.du.executor.workspace import HttpWorkspace
import unittest


class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def testName(self):
        home_path="d:/"
        name="dataming"
        workspace = HttpWorkspace(home_path,name)
        files_dict = {'dataming/index.html':'http://localhost:8161/',
                      'datami/index.html':'http://localhost:8161/',
                      'datai/index.html':'http://localhost:8161/',
                      'datami/inex.html':'http://localhost:8161/'}
        workspace.init_files(files_dict)
        operation = Operation(name="svncommand")
        operation.add_command("ls")
        results = workspace.execute(operation)
        for key in results.keys():
            print results[key][0].decode('gbk').encode('utf-8')
        workspace.clean()
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()